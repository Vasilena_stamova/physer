import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HighScoreComponent} from './high_score/hs.component';
import {GameComponent} from './game/game.component';
import { NavigationComponent } from './navigation/navigation.component';

const routes: Routes = [
{ path: '', component: NavigationComponent },
    { path: 'high_score', component: HighScoreComponent},
    { path: 'game', component: GameComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
