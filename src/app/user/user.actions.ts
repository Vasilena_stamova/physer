import { Player } from './../user/user.model'

export class AddPlayer {
    static readonly type = '[Name] Add'

    constructor(public payload: Player ) {}
}

export class RemovePlayer {
    static readonly type = '[Name] Remove'

    constructor(public payload: string) {}
}

export class  ChangeScore{
    static readonly type = 'Score: '
    constructor(public payload: Player) {}

}