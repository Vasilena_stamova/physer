import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Player } from './../user/user.model'
import { AddPlayer, ChangeScore, RemovePlayer } from './../user/user.actions'


export class PlayerStateModel {
    players: Player[];
}

@State<PlayerStateModel>({
    name: 'players',
    defaults: {
       players: []
    }
})
export class PlayerState {

    
    @Selector()
    static getPlayer(state: PlayerStateModel) {
        return state.players
    }

   
    @Action(AddPlayer)
    add({getState, patchState }: StateContext<PlayerStateModel>, { payload }:AddPlayer) {
        const state = getState();
        patchState({
            players: [...state.players, payload]
        })
    }

    @Action(RemovePlayer)
    remove({getState, patchState }: StateContext<PlayerStateModel>, { payload }:RemovePlayer) {
        patchState({
            players: getState().players.filter(players => players.name != payload)
        })
    }
    @Action(ChangeScore)
    change({getState, patchState }: StateContext<PlayerStateModel>, { payload }:ChangeScore) {
        const players = getState().players;
        const playerIndex = players.findIndex((player: Player) => player.name == payload.name);

        console.log(players, payload);
        players[playerIndex] = payload;
        
        patchState({
            players 
        })
    }
}   