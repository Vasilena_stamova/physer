import { Component } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({ 
    selector: 'app-high-score',
    templateUrl: './hs.component.html' ,
    styleUrls: ['./hs.component.css']
})

export class HighScoreComponent { 
    globalThis;
    players;

    constructor(private store: Store){
        this.globalThis=globalThis;

        this.store.select(state => state.players.players)
            .subscribe((result) => {
                this.players = [...result.sort((a, b) => b.score - a.score)].splice(0, 10);
                console.log(this.players)
            });
    }
}