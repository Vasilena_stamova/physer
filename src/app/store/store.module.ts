import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxsModule} from '@ngxs/store';
import {environment as env} from '../../environments/environment';
import {NgxsStoragePluginModule} from '@ngxs/storage-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forRoot([],{developmentMode: !env.production}),
    NgxsStoragePluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot({logger:console, collapsed:false, disabled: env.production})

  ]
})
export class StoreModule { }
