import { Component, OnInit } from '@angular/core';
import {Store, Select} from '@ngxs/store';
import { AddPlayer, } from '../user/user.actions';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  constructor(private store: Store) { 
    globalThis.storeRef = this.store;
  }

  addPlayer(name) {
    const currentUser = {name: name, score: 0};
    this.store.dispatch(new AddPlayer(currentUser)).subscribe(() => {
        globalThis.user = currentUser;
        console.log('globalThis.user => ', globalThis.user);
    });
  }

}
