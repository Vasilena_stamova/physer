import {Component, OnInit} from '@angular/core';
import {AUTO, Game, Math, Phaser} from '../../assets/Phaser.js';
import {Store} from '@ngxs/store';
import {Observable } from 'rxjs';
import {ChangeScore } from '../user/user.actions';
import { Player } from '../user/user.model';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
    user$: Observable<Player>;
    phaserGame: Phaser.Game;
    config: Phaser.Types.Core.GameConfig;

    bombs;
    stars;
    player;
    platforms;
    cursors: Phaser.Types.Input.keyboard.CursorKeys; 
    gameOver = false;
    anims;
    score = 0;

    constructor(private store: Store) {
        globalThis.storeRef = this.store;

        this.config = {
            type: AUTO,
            width: 800,
            height: 600,
            scene: {
                preload,
                create,
                update
            },
            parent: 'gameContainer',
            physics: {
                default: 'arcade',
                arcade: {
                    gravity: {y: 100},
                }
            },
        }
    }

    //addPlayer(name) {
    //    const currentUser = {name: name, score: 0};
     //   this.store.dispatch(new AddPlayer(currentUser)).subscribe(() => {
      //      globalThis.user = currentUser;
      //      console.log('globalThis.user => ', globalThis.user);
      //  });
     // }

    changeScore(player){
        this.store.dispatch(new ChangeScore(player))
    }

    ngOnInit() {
        this.phaserGame = new Game(this.config);
    
    }
}


function hitBomb(player, bomb) {
    this.physics.pause();
    this.player.setTint(0xff0000);
    this.player.anims.play('turn');

    globalThis.location.href = '/high_score';
}
    


function collectStar(player, star) {
    star.disableBody(true, true);
    console.log('globalThis.user => ', globalThis.user);
    globalThis.storeRef.dispatch(new ChangeScore({name: globalThis.user.name, score: globalThis.user.score + 10}));
    globalThis.user.score += 10;
   this.scoreText.setText('Score: ' + globalThis.user.score);
   

    if (this.stars.countActive(true) === 0) {
        this.stars.children.iterate(child => {

            child.enableBody(true, child.x, 0, true, true);

        });

        const x = (this.player.x < 400) ? Math.Between(400, 800) : Math.Between(0, 400);
        const bomb = this.bombs.create(x, 16, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Math.Between(-200, 200), 20);
        bomb.allowGravity = false;

    }
}

function preload() {
    this.load.image('sky', 'assets/sky.png');
    this.load.image('ground', 'assets/platform.png');
    this.load.image('bomb', 'assets/bomb.png');
    this.load.spritesheet('dude', 'assets/dude.png', {frameWidth: 32, frameHeight: 48});
    this.load.image('star', 'assets/star.png', {
        frameWidth: 32, frameHeight: 48
    });
}

function create() {
    this.add.image(400, 300, 'sky');

    //  The platforms group contains the ground and the 2 ledges we can jump on
    this.platforms = this.physics.add.staticGroup();

    this.platforms.create(400, 568, 'ground').setScale(2).refreshBody();

    //  Now let's create some ledges
    this.platforms.create(600, 400, 'ground');
    this.platforms.create(50, 250, 'ground');
    this.platforms.create(750, 220, 'ground');
    
    //  The score
   this.scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' }) 

    this.player = this.physics.add.sprite(100, 450, 'dude');
    this.player.setBounce(0.2);
    this.player.setCollideWorldBounds(true);

    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dude', {start: 0, end: 3}),
        frameRate: 20,
        repeat: -1
    });

    this.anims.create({
        key: 'turn',
        frames: [{key: 'dude', frame: 4}],
        frameRate: 30
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dude', {start: 5, end: 8}),
        frameRate: 20,
        repeat: -1
    });

    //  Input Events
    this.cursors = this.input.keyboard.createCursorKeys();

    this.stars = this.physics.add.group({
        key: 'star',
        repeat: 11,
        setXY: {x: 12, y: 0, stepX: 70}
    });

    this.stars.children.iterate(function (child) {

       
        child.setBounceY(Math.FloatBetween(0.4, 0.8));

    });

    this.bombs = this.physics.add.group();

    //  Collide the player and the stars with the platforms
    this.physics.add.collider(this.player, this.platforms);
    this.physics.add.collider(this.stars, this.platforms);
    this.physics.add.collider(this.bombs, this.platforms);

    //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
    this.physics.add.overlap(this.player, this.stars, collectStar, null, this);

    this.physics.add.collider(this.player, this.bombs, hitBomb, null, this);

    //this.store.dispatch(new User('Vasilena', 0));
}

function update() {
    if (this.gameOver) {
        return;
    }

    if (this.cursors.left.isDown) {
        this.player.setVelocityX(-160);

        this.player.anims.play('left', true);
    } else if (this.cursors.right.isDown) {
        this.player.setVelocityX(160);

        this.player.anims.play('right', true);
    } else {
        this.player.setVelocityX(0);

        this.player.anims.play('turn');
    }

    if (this.cursors.up.isDown && this.player.body.touching.down) {
        this.player.setVelocityY(-230);
    }
}
